package com.ecommerce.Controller;

import com.ecommerce.MyExceptions.incorrectQuantityException;
import com.ecommerce.data.ItemData;
import com.ecommerce.entity.Item;
import com.ecommerce.repository.ItemImpl;
import com.ecommerce.repository.addToCartImpl;
import com.ecommerce.entity.addToCart;

import java.util.*;


public class Main {
    static Scanner sc=new Scanner(System.in);
    public static void main(String args[])
    {
        ItemData itemdata=new ItemData();
        System.out.println("\n\n\n************************************************** WELCOME TO AMAZON SHOPPING ZONE **************************************************");

        while (true)
        {
            System.out.println("\n\n1.List All Items    2.Search from all items     3.Filter    4.Buy    5.Add_To_Cart     6.ShowAddToCart     7.Buy_All_From_Add_To_Cart      8.Exit");
            try {
                int option = sc.nextInt();
                if (option > 8)
                    throw new InputMismatchException("\n\t\t\tYou selected invalid option. Please select a Proper Option : \n");
                chooseOption(option);
            }
            catch (InputMismatchException e) {
                System.out.println(e);
                sc.nextLine();
            }
        }
    }


    private static void chooseOption(int option)
    {
        switch (option)
        {
            case 1:
                listAllItems();
                break;

            case 2:
                System.out.println("\nEnter item Name : ");
                String searchItemName = sc.next();
                System.out.printf("%5s %30s %20s %30s %17s %22s", "ITEM ID", "ITEM NAME", "PRICE", "CUSTOMER RATING", "COLOR", "CATEGORY\n");
                System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
                ItemImpl.getInstance().searchItem(searchItemName);
                break;

            case 3:
                filterItems();
                break;

            case 4:
                System.out.println("\nEnter Item Id and Quantity");
                int itemNum = sc.nextInt();
                int quantity = sc.nextInt();
                try {
                    ItemImpl.getInstance().buyItems(itemNum, quantity);
                }
                catch(incorrectQuantityException e){
                    System.out.println(e);
                }
                break;

            case 5:
                System.out.println("\nEnter ItemID : ");
                int itemId = sc.nextInt();
                int itemQuantity = sc.nextInt();
                addToCart cart = new addToCart(itemId,itemQuantity);
                addToCartImpl.getInstance().save(cart);
                break;

            case 6:
                addToCartImpl.getInstance().showAllItems();
                break;
            case 7:
                double totalAmount = addToCartImpl.getInstance().buyAll();
                System.out.println("TotalAmount : "+totalAmount+" Rs.");
                break;
            case 8:
                System.exit(0);
        }
    }

    // listing all the items both electronics and fasion.
    private static void listAllItems()
    {
        List<Item> items= ItemImpl.getInstance().getAllItems();
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%5s %30s %20s %30s %17s %22s", "ITEM ID", "ITEM NAME", "PRICE", "CUSTOMER RATING", "COLOR", "CATEGORY\n");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
        for(Item item:items)
        {
            System.out.println(item);
        }
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");

    }

    //Initially i stored data of both electrical and fasion in a single arraylist with attribute Category. here when user pressed on filter 1st filter is based on electrical or fasion
    private static void filterItems()
    {
        List<Item> filteredItems = null;
        System.out.println("\n1 : Electronics    2 : Fasion  | Select option : ");
        int choice = sc.nextInt();
        if(choice == 1) {
            filteredItems = ItemImpl.getInstance().getItemByCategory("Electronics");//base on electronics
        }
        else {
            filteredItems = ItemImpl.getInstance().getItemByCategory("Fashion");//based on fasion
        }

        System.out.println("\n1:BasedOnPrice   2:BasedOnColor   3:BasedOnCustemerRatings   | Select option : ");
        choice = sc.nextInt();
        if(choice == 1)
            ItemImpl.getInstance().filterOnPrice(filteredItems);//filtering based on the price by passing filtered items list.
        else if(choice == 2)
            ItemImpl.getInstance().filterOnColor(filteredItems);
        else
            ItemImpl.getInstance().filterOnCustomerRating(filteredItems);

    }



}
