package com.ecommerce.repository;

import com.ecommerce.MyExceptions.incorrectQuantityException;
import com.ecommerce.entity.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ItemImpl implements ItemDictionary {
    float searchRating;
    int totalAmount;
    ArrayList<Item> itemContainer=new ArrayList<Item>();
    Scanner s = new Scanner(System.in);
    private ItemImpl(){}
    private static ItemImpl SINGLE_INSTANCE=null;

    //This is because i don't need to create much objects...whenever object is required this method will return the same object is which is already created.
    public static ItemImpl getInstance()
    {
        if(SINGLE_INSTANCE==null)
            SINGLE_INSTANCE=new ItemImpl();
        return SINGLE_INSTANCE;

    }

    @Override
    public List<Item> getAllItems() {
        return itemContainer;
    } //returning Arraylist.

    @Override
    public void save(Item item) {
    itemContainer.add(item);
    }//adding items to Arraylist.

    //Filtering on filteredItems(Contains either electronics or fasion items).
    @Override
    public void filterOnPrice(List<Item> filteredItems) {
        System.out.println("\nEnter Price : ");
        int itemPrice = s.nextInt();
        System.out.printf("%5s %30s %20s %30s %17s %22s", "ITEM ID", "ITEM NAME", "PRICE", "CUSTOMER RATING", "COLOR", "CATEGORY\n");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
        for(Item item:filteredItems)
        {
            if(item.getPrice() <= itemPrice )
                System.out.println(item);
        }
    }

    //Buy based on itemid(if item id is equal to item in list) will retrieve the price of that perticular item and calculate the price.
    @Override
    public void buyItems(int itemNum, int quantity) throws incorrectQuantityException {
        if(quantity == 0)
            throw new incorrectQuantityException("Please select proper quantity.");
        for(Item item : itemContainer)
        {
            if(item.getNumber() == itemNum && item.getQuantity() < quantity)
                throw new incorrectQuantityException("\nSorry..... We dont have "+quantity+" quantity at this time.");
        }
        for(Item item:itemContainer)
        {
            if(item.getNumber() == itemNum) {
                totalAmount += item.getPrice() * quantity;
                System.out.println("\nTotal Amount : " + totalAmount);
                break;
            }
        }
    }


    @Override
    public void filterOnColor(List<Item> filteredItems) {
        System.out.println("\nEnter Color : ");
        String itemColor = s.next();
        System.out.printf("%5s %30s %20s %30s %17s %22s", "ITEM ID", "ITEM NAME", "PRICE", "CUSTOMER RATING", "COLOR", "CATEGORY\n");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
        for(Item item:filteredItems)
        {
            if(item.getColor().equals(itemColor))
                System.out.println(item);
        }
    }

    @Override
    public void filterOnCustomerRating(List<Item> filteredItems) {
        System.out.println("\nSelect Rating [ 1 2 . . . 5 ] ");
        searchRating = s.nextFloat();
        System.out.printf("%5s %30s %20s %30s %17s %22s", "ITEM ID", "ITEM NAME", "PRICE", "CUSTOMER RATING", "COLOR", "CATEGORY\n");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
        for (Item item : filteredItems)
        {
            if(item.getCustomerRating() > searchRating)
                System.out.println(item);
        }
    }

    //This function for searching particular item. because suppose if we have a much data then searching one by one is difficult so that by item name we can search.
    @Override
    public void searchItem(String itemName) {
        ArrayList<Item> filterItems = new ArrayList<Item>();
        for(Item item:itemContainer)
        {
            if(item.getName().toUpperCase().contains(itemName.toUpperCase()))
                System.out.println(item);
        }
    }

    //Return Either Electronics or Fasion items..based on comparision of category.
    @Override
    public List<Item> getItemByCategory(String category) {
        ArrayList<Item> filterItemContainer=new ArrayList<Item>();
        for(Item item:itemContainer)
        {
            if(item.getCategory().equals(category))
            {
                filterItemContainer.add(item);
            }
        }
        return filterItemContainer;
    }

    @Override
    public Item getItemById(int id) {
        Item item=null;
        for(Item tempItem:itemContainer)
        {
            if(tempItem.getNumber()==id)
                item=tempItem;
        }
        return item;
    }
}
