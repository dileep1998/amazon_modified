package com.ecommerce.repository;

import com.ecommerce.entity.Item;
import com.ecommerce.entity.addToCart;
import com.ecommerce.repository.ItemImpl;

import java.util.ArrayList;


public class addToCartImpl {

    ArrayList<addToCart> cartContainer = new ArrayList<addToCart>();
    double totalAmount;
    private addToCartImpl(){

    }

    private static addToCartImpl SINGLE_INSTANCE = null;

    public static addToCartImpl getInstance()
    {
        if(SINGLE_INSTANCE == null)
            SINGLE_INSTANCE = new addToCartImpl();
        return SINGLE_INSTANCE;
    }

    public void save(addToCart item) {
        cartContainer.add(item);
        for(Item res :ItemImpl.getInstance().itemContainer)
        {
            if(item.getItemId() == res.getNumber())
                totalAmount += res.getPrice()*item.getQuantity();
        }
    }

    public void showAllItems(){
        for(addToCart item : cartContainer)
            System.out.println("ItemId : "+item.getItemId()+" Quantity : "+item.getQuantity());
    }

    public double buyAll(){
        return totalAmount;
    }


}
