package com.ecommerce.repository;

import com.ecommerce.MyExceptions.incorrectQuantityException;
import com.ecommerce.entity.Item;

import java.util.List;

public interface ItemDictionary {
    public List<Item> getAllItems();
    public void save(Item item);
    public List<Item> getItemByCategory(String category);
    public void searchItem(String itemName);
    public void filterOnPrice(List<Item> filteredItems);
    public void filterOnColor(List<Item> filteredItems);
    public void filterOnCustomerRating(List<Item> filteredItems);
    public void buyItems(int itemNum, int quantity) throws incorrectQuantityException;
    public Item getItemById(int id);
}
