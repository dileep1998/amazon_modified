package com.ecommerce.entity;

public class Item {
    private int number;
    private String name;
    private double price;
    private float customerRating;
    private String category;
    private String color;
    private int quantity;


    public Item(int number, String name, double price, String category, String color, float rating,int quantity) {
        this.number = number;
        this.name = name;
        this.price = price;
        this.category=category;
        this.color = color;
        this.customerRating = rating;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getColor() {
        return color;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public float getCustomerRating() {
        return customerRating;
    }

    public String getCategory() {
        return category;
    }


    @Override
    public String toString() {
        return String.format("%5d\t%30s\t%20s\t%20s\t%20s\t%20s",number,name,price,customerRating,color,category);
    }
}
