package com.ecommerce.entity;

public class addToCart {
    private int itemId;
    private int quantity;


    public addToCart(int cartItemId,int cartItemQuantity)
    {
        this.itemId = cartItemId;
        this.quantity = cartItemQuantity;
    }
    //getters
    public int getItemId() {
        return itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    //setters


    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
