package com.ecommerce.data;


import com.ecommerce.entity.Item;
import com.ecommerce.repository.ItemImpl;

import java.util.ArrayList;
import java.util.List;

public class ItemData {
    public ItemData()
    {

        Item computer1=new Item(101,"LG_Computer",29000,"Electronics","Black", (float) 3.5,2);
        Item computer2=new Item(102,"DELL_Computer",39000,"Electronics","Black",(float) 4.5,3);
        Item laptop1=new Item(103,"HP_Laptop",32000,"Electronics","White",(float) 3.6,9);
        Item laptop2=new Item(104,"Lenovo_Laptop",42000,"Electronics","Black",(float) 4.2,8);
        Item mobile1=new Item(105,"VIVO_V11",32000,"Electronics","Pink",(float) 5.0,7);
        Item mobile2=new Item(106,"Redmi_11",19000,"Electronics","Yellow",(float) 3.5,8);

        ItemImpl.getInstance().save(computer1);
        ItemImpl.getInstance().save(computer2);
        ItemImpl.getInstance().save(laptop1);
        ItemImpl.getInstance().save(laptop2);
        ItemImpl.getInstance().save(mobile1);
        ItemImpl.getInstance().save(mobile2);

        Item shirt1=new Item(107,"LP_shirt",3000,"Fashion","Red",(float) 3.5,9);
        Item shirt2=new Item(108,"T_shirt",500,"Fashion","Blue",(float) 3.0,6);
        Item shirt3=new Item(109,"Ralphlauren_shirt",1000,"Fashion","Black",(float) 4.5,5);
        Item shoe1=new Item(110,"Nike",3100,"Fashion","Grey",(float) 4.0,7);
        Item shoe2=new Item(111,"NewBalance",4000,"Fashion","Black",(float) 2.5,8);
        Item pant=new Item(112,"FormalPant",2000,"Fashion","Black",(float) 3.8,9);

        ItemImpl.getInstance().save(shirt1);
        ItemImpl.getInstance().save(shirt2);
        ItemImpl.getInstance().save(shirt3);
        ItemImpl.getInstance().save(shoe1);
        ItemImpl.getInstance().save(shoe2);
        ItemImpl.getInstance().save(pant);

    }
}
